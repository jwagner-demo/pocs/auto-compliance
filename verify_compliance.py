import requests
import os
import json

# Get The API Token From The Project CI / CD Variables
token = os.environ.get("GRAPHQL_API_TOKEN")

# URL To Your GraphQL API
url = 'https://gitlab.com/api/graphql'

# Your Compliance Framework Global ID To Add
compliance_framework_id = "gid://gitlab/ComplianceManagement::Framework/2911"

# The Path Of The Group You Want To Update All Projects Under
group_path = "jwagner-demo/pocs"

# GraphQL Query To Get List Of Projects In Group
# TODO: This is a hack to insert variables into a multi line string
project_query = """
  query {
    group(fullPath: "<group-path>") {
      projects {
        nodes {
          id
        }
      }
    }
  }
"""
project_query = project_query.replace("<group-path>",group_path)

# Set Headers And Make THe Request
headers = {"Authorization": f'Bearer {token}'}
project_request = requests.post(url, json={'query': project_query}, headers=headers)

# Convert The Response To JSON
json_data = json.loads(project_request.text)

# Get The List Of IDs
id_list = json_data["data"]["group"]["projects"]["nodes"]

# For Each Project ID In The List
for item in id_list:
  project_id = item["id"]

  # GraphQL Mutation To Add The Compliance Framework
  # TODO: This is a hack to insert variables into a multi line string
  mutation = """
    mutation {
      projectSetComplianceFramework(
        input: 
          { 
            clientMutationId: "Automatic add of the compliance framework",
            complianceFrameworkId: "<compliance-id>",
            projectId: "<project-id>"
          }
        ) {
        project{
          name
        }
        errors
      }
    }
  """
  mutation = mutation.replace("<project-id>", project_id)
  mutation = mutation.replace("<compliance-id>", compliance_framework_id)

  # Add Headers And Make The Request
  headers = {"Authorization": f'Bearer {token}'}
  r = requests.post(url, json={'query': mutation}, headers=headers)

  # Print Results
  print(r.status_code)
  print(r.text)
