# auto-compliance

This project contains code that will set the Compliance Framework from a .gitlab-ci.yml file for a Project so that it can be used as part of a scheduled pipeline to ensure all projects follow a specific compliance framework.

## Working GraphQL Query

**Mutation**

The [projectSetComplianceFramework](https://docs.gitlab.com/ee/api/graphql/reference/#mutationprojectsetcomplianceframework) mutaiton is used to assign a compliance framework for a project.

**Input**
- _clientMutationId_ - A unique identifier for the client performing the mutation.

- _complianceFrameworkId_ - ID of the compliance framework to assign to the project. Set to null to unset. This is a [ComplianceManagementFrameworkId](https://docs.gitlab.com/ee/api/graphql/reference/#compliancemanagementframeworkid) which is a [global ID](https://docs.gitlab.com/ee/api/graphql/getting_started.html#queries-and-mutations) encoded as a string.

- _projectId_ - ID of the project to change the compliance framework of. This [ProjectID](https://docs.gitlab.com/ee/api/graphql/reference/#payloadalertfieldpathsegment) which is a [global ID](https://docs.gitlab.com/ee/api/graphql/getting_started.html#queries-and-mutations) encoded as a string.


```
mutation {
  projectSetComplianceFramework(
    input: 
    	{ 
        clientMutationId: "custom mutation",
      	complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/2911",
        projectId: "gid://gitlab/Project/40369064"
    	}
  	) {
    project{
      name
    }
    errors
  }
}
```

